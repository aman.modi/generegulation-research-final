# baseimage from: https://hub.docker.com/r/tensorflow/tensorflow/
FROM ubuntu:20.04

# install git + python3.8
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install software-properties-common -y
RUN apt-get install git -y
RUN apt-get install python3 pip -y
RUN apt-get install wget tar -y

# install cuda
RUN wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
RUN mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
RUN wget https://developer.download.nvidia.com/compute/cuda/11.2.0/local_installers/cuda-repo-ubuntu2004-11-2-local_11.2.0-460.27.04-1_amd64.deb
RUN dpkg -i cuda-repo-ubuntu2004-11-2-local_11.2.0-460.27.04-1_amd64.deb
RUN apt-key add /var/cuda-repo-ubuntu2004-11-2-local/7fa2af80.pub
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install tzdata
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install keyboard-configuration
RUN apt-get -y install cuda

# install code dependencies
RUN pip install sklearn pandas scipy keras matplotlib tensorflow biopython pyyaml scikeras

# get tensorflow working on the gpu
#RUN ln -s /usr/local/cuda-11.0/targets/x86_64-linux/lib/libcusolver.so.10 $(python3 -c "import tensorflow.python as x; print(x.__path__[0])")/libcusolver.so.11
RUN ln -s /usr/local/cuda-11.2/targets/x86_64-linux/lib/libcusolver.so.10 $(python3 -c "import tensorflow.python as x; print(x.__path__[0])")/libcusolver.so.11

RUN wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin

RUN mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
RUN apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/7fa2af80.pub
RUN add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ /"
RUN apt-get update

RUN apt-get install libcudnn8=8.1.1.*-1+cuda11.2 -y
RUN apt-get install libcudnn8-dev=8.1.1.*-1+cuda11.2 -y


RUN apt-get install nano -y