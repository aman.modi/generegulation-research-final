from sklearn.model_selection import train_test_split
import numpy as np

def train_test_split_data(X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, random_state=42)
    X_train = X_train.to_numpy()
    X_test = X_test.to_numpy()
    X_train = X_train.reshape(X_train.shape[0], X_train.shape[1])
    X_test = X_test.reshape(X_test.shape[0], X_test.shape[1])
    return X_train, X_test, y_train, y_test