from Bio import SeqIO
import pandas as pd
from keras.losses import mse
from sklearn.metrics import accuracy_score
from train_test_split import train_test_split_data
from model import create_model
import os
from scikeras.wrappers import KerasClassifier
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from keras import backend as K
from tensorflow.keras.models import load_model
from figure import plot_metric
from keras.callbacks import EarlyStopping, ModelCheckpoint
from load_config_file import load_config

K.clear_session()
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
early_stopping = EarlyStopping()

config = load_config("config.yaml")

# reading the fasta file
rows = []
with open(os.path.join(config['data_directory'], config['data_name'])) as fasta_file:
    for seq_record in SeqIO.parse(fasta_file, 'fasta'):
        index, output = seq_record.id.split('_')
        rows.append([seq_record.seq, output])

# creating the dataframe using sequences and labels
df = pd.DataFrame(rows, columns=['sequence', 'label'])
df = df.replace({'positive': 1, 'negative': 0})
sequences = list(df['sequence'])
classes = list(df['label'])

# using each base as a separate column generate a new dataframe
dataset = {}
for i, sequence in enumerate(sequences):
    nucleosome = list(sequence)
    nucleosome = [x for x in nucleosome]
    nucleosome.append(classes[i])
    dataset[i] = nucleosome
data = pd.DataFrame(dataset)
data = data.T
data.rename(columns={147: 'label'}, inplace=True)
data_new = pd.get_dummies(data)
data_new = data_new.drop(columns=['label_0'])
data_new.rename(columns={'label_1': 'label'}, inplace=True)

print(data_new.head())

# splitting the data into X and y
# X = data_new.drop(['label'], axis=1)
# y = data_new['label']
#
# # generating train and test sets from the data
# X_train, X_test, y_train, y_test = train_test_split_data(X, y)
#
# # building the model
# filepath = 'my_best_model.hdf5'
# checkpoint = ModelCheckpoint(filepath=filepath,
#                              monitor='val_loss',
#                              verbose=1,
#                              save_best_only=True,
#                              mode='min')
# callbacks = [checkpoint]
# model = create_model(dropout=0.2, learning_rate=0.0001)
# history = model.fit(X_train, y_train, validation_split=0.1, batch_size=64, epochs=100, callbacks=callbacks)
# plot_metric(history, 'accuracy')
# model = load_model(filepath)
# yhat = model.predict(X_test)
# print('Model MSE on test data = ', mse(y_test, yhat).numpy())