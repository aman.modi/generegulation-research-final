import pandas as pd
from sklearn.model_selection import train_test_split
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense, Input, Conv1D, Flatten, MaxPooling1D, Dropout
from tensorflow.keras.optimizers import Adam
from model import create_model
from scikeras.wrappers import KerasClassifier
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV

df = pd.read_csv('../data/nucleosome_feature_martix.tsv', sep='\t')
df = df.replace({'Positive': 1, 'Negative': 0})

X = df[df.columns.drop('label')]
y = df['label']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, random_state=7)
X_train = X_train.to_numpy()
X_test = X_test.to_numpy()
X_train = X_train.reshape(X_train.shape[0], X_train.shape[1],1)
X_test = X_test.reshape(X_test.shape[0], X_test.shape[1], 1)

model = KerasClassifier(model=create_model, learning_rate=0.0)
batch_size = [128, 256, 512, 1024]
epochs = [50, 100]
learning_rate = [0.1, 0.01, 0.001, 0.0001, 0.00001]
param_grid = dict(batch_size=batch_size, epochs=epochs, learning_rate=learning_rate)
grid = RandomizedSearchCV(estimator=model, param_distributions=param_grid, n_jobs=1, cv=3, pre_dispatch='2*n_jobs')
grid_result = grid.fit(X_train, y_train)
# summarize results
print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))
print('Evaluating best model:')
bestModel = grid_result.best_estimator_
accuracy = bestModel.score(X_test, y_test)
print('accuracy: {:.2f}%'.format(accuracy*100))


