import pandas as pd
from sklearn.preprocessing import StandardScaler, MinMaxScaler

def create_files_for_channels_new(names):
    l = list()
    counter = 0
    # scaler = MinMaxScaler()
    for name in names:
        file = open(f'../data/nucleosome_guo2014.fa.{name}').read()
        ids = []
        values = []
        newline = []
        a = file.split('>')
        for i in range(1, len(a)):
            a[i] = a[i].split('\n')
            ids.append(a[i][0].split('_')[1])
            for j in range(1, len(a[i])):
                newline.append(a[i][j])
            values.append(newline)
            newline = []
        newlist = ''
        anotherlist = []
        for j in range(len(values)):
            for i in range(len(values[j])):
                newlist = newlist + ',' + values[j][i]
            anotherlist.append([newlist])
            newlist = ''
        for i in range(len(anotherlist)):
            anotherlist[i] = [anotherlist[i][0].replace('NA', '')]
        finallist = []
        yetanotherlist = []
        for i in range(len(anotherlist)):
            anotherlist[i] = anotherlist[i][0].split(',')
            for j in anotherlist[i]:
                if len(j) != 0:
                    yetanotherlist.append(float(j))
            finallist.append(yetanotherlist)
            yetanotherlist = []
        # finallist = scaler.fit_transform(finallist)
        # finallist = finallist.tolist()
        # print(len(finallist[0]))
        # for i in range(len(finallist)):
        #     if len(finallist[i]) != 147:
        #         length = 147-len(finallist[i])
        #         for j in range(length):
        #             finallist[i].append(float(0))
        for i in range(len(finallist)):
            if len(finallist[i]) != 144:
                finallist[i].append(float(0))
        name = pd.DataFrame(finallist)
        name.columns = [names[counter] + str(i) for i in range(0, len(name.columns))]
        l.append(name)
        counter += 1
    return l, ids


names = ['HelT', 'MGW', 'ProT', 'Roll']

properties, labels = create_files_for_channels_new(names)
print(labels)

