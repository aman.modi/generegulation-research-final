import yaml
import os

# reading the config file
CONFIG_PATH = '../config/'


# function to load the config file
def load_config(config_name):
    with open(os.path.join(CONFIG_PATH, config_name)) as file:
        config = yaml.safe_load(file)
    return config
