import pandas as pd
import numpy as np
from new_channels import create_files_for_channels_new
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv1D, Flatten, MaxPooling1D, Dropout
from tensorflow.keras.optimizers import Adam, SGD
from scikeras.wrappers import KerasClassifier
from sklearn.model_selection import RandomizedSearchCV
from sklearn.preprocessing import StandardScaler, MinMaxScaler


names = ['HelT', 'MGW', 'ProT', 'Roll']

properties, labels = create_files_for_channels_new(names)
stacked_array = np.dstack((properties[0], properties[1], properties[2], properties[3]))
labels = [1 if labels[label] == 'positive' else 0 for label in range(len(labels))]
X_train, X_test, y_train, y_test = train_test_split(stacked_array, labels, test_size=0.1, random_state=7)


print(X_train[0])

scaler = MinMaxScaler()
for i in range(X_train.shape[0]):
    X_train[i] = scaler.fit_transform(X_train[i])
for i in range(X_test.shape[0]):
    X_test[i] = scaler.transform(X_test[i])

print(X_train[0])

X_train = np.array(X_train)
X_test = np.array(X_test)
y_train = np.array(y_train)
y_test = np.array(y_test)


def create_model(learning_rate):
    model = Sequential()
    model.add(Conv1D(256, kernel_size=3, input_shape=(144,4), activation='relu'))
    model.add(MaxPooling1D())
    model.add(Conv1D(128, kernel_size=3, activation='tanh'))
    model.add(MaxPooling1D())
    model.add(Conv1D(64, kernel_size=3, activation='relu'))
    model.add(MaxPooling1D())
    model.add(Conv1D(32, kernel_size=3, activation='relu'))
    model.add(MaxPooling1D())
    model.add(Conv1D(16, kernel_size=3, activation='tanh'))
    model.add(MaxPooling1D())
    model.add(Flatten())
    model.add(Dense(8, activation='relu'))
    model.add(Dense(4, activation='tanh'))
    model.add(Dense(4, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    optimizer = SGD(learning_rate=learning_rate)
    model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    return model

model = KerasClassifier(model=create_model, learning_rate=0.0)
batch_size = [16, 32, 64, 128, 256, 512, 1024]
epochs = [10, 20, 50, 100]
learning_rate = [0.1, 0.01, 0.001, 0.0001, 0.00001]
param_grid = dict(batch_size=batch_size, epochs=epochs, learning_rate=learning_rate)
grid = RandomizedSearchCV(estimator=model, param_distributions=param_grid, n_jobs=1, cv=3, pre_dispatch='2*n_jobs')
grid_result = grid.fit(X_train, y_train)

print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))
print('Evaluating best model:')
bestModel = grid_result.best_estimator_
accuracy = bestModel.score(X_test, y_test)
print('accuracy: {:.2f}%'.format(accuracy*100))

# model = create_model(learning_rate=0.01)
# history = model.fit(X_train,y_train, epochs=100,batch_size=64, validation_data=(X_test,y_test))
# _, acc = model.evaluate(X_test, y_test, verbose=0)
# print('> %.3f' % (acc * 100.0))