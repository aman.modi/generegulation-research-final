import os
from load_config_file import load_config
import pandas as pd

directories = ['../data/nucleosome_guo2014.fa.HelT', '../data/nucleosome_guo2014.fa.MGW',
               '../data/nucleosome_guo2014.fa.ProT', '../data/nucleosome_guo2014.fa.Roll']
names = ['HelT', 'MGW', 'ProT', 'Roll']
def read_file(files, names):
    final_df = pd.DataFrame()
    for index, f in enumerate(files):
        file = open(f).read()
        ids = []
        values = []
        newline = []
        a = file.split('>')
        for i in range(1, len(a)):
            a[i] = a[i].split('\n')
            ids.append(a[i][0].split('_')[1])
            for j in range(1, len(a[i])):
                newline.append(a[i][j])
            values.append(newline)
            newline = []
        newlist = ''
        anotherlist = []
        for j in range(len(values)):
            for i in range(len(values[j])):
                newlist = newlist + ',' + values[j][i]
            anotherlist.append([newlist])
            newlist = ''
        for i in range(len(anotherlist)):
            anotherlist[i] = [anotherlist[i][0].replace('NA', '')]
        finallist = []
        yetanotherlist = []
        for i in range(len(anotherlist)):
            anotherlist[i] = anotherlist[i][0].split(',')
            for j in anotherlist[i]:
                if len(j) != 0:
                    yetanotherlist.append(float(j))
            finallist.append(yetanotherlist)
            yetanotherlist = []
        df= pd.DataFrame(finallist)
        df.columns = [names[index] + str(i) for i in range(0, len(df.columns))]
        final_df = pd.concat([final_df, df], axis=1)
    df1 = pd.DataFrame(ids, columns=['labels'])
    final_df = pd.concat([final_df, df1], axis=1)
    final_df.to_csv('../data/nucleosome_guo2014.csv', index=False)
    print(final_df)

read_file(directories, names)
