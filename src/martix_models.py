import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv1D, Flatten, MaxPooling1D, Dropout
from tensorflow.keras.optimizers import Adam, SGD
from scikeras.wrappers import KerasClassifier
from sklearn.model_selection import RandomizedSearchCV
from sklearn.preprocessing import StandardScaler, MinMaxScaler

data_file = '../data/nucleosome_feature_martix.tsv'
df = pd.read_csv(data_file, sep='\t')
df = df.replace({'Positive': 1, 'Negative': 0})

X = df[df.columns.drop('label')]
y = df['label']
new_df = pd.DataFrame()

split_factor = len(X.columns)/4
counter = 1

new_df1 = X.iloc[:, :50]
new_df2 = X.iloc[:, 50:100]
new_df3 = X.iloc[:, 100:150]
new_df4 = X.iloc[:, 150:200]

df_list = [new_df1, new_df2, new_df3, new_df4]
l = list()
labels = list()

for d in df_list:
    l.append(d)

for i in range(len(y)):
    labels.append(y[i])

stacked_array = np.dstack((l[0], l[1], l[2], l[3]))
X_train, X_test, y_train, y_test = train_test_split(stacked_array, labels, test_size=0.1, random_state=42)

X_train = np.array(X_train)
X_test = np.array(X_test)
y_train = np.array(y_train)
y_test = np.array(y_test)

def create_model(learning_rate):
    model = Sequential()
    model.add(Conv1D(128, kernel_size=3, input_shape=(50,4), activation='relu'))
    model.add(MaxPooling1D())
    model.add(Conv1D(64, kernel_size=3, activation='tanh'))
    model.add(MaxPooling1D())
    model.add(Conv1D(32, kernel_size=3, activation='relu'))
    model.add(MaxPooling1D())
    model.add(Conv1D(16, kernel_size=3, activation='tanh'))
    model.add(MaxPooling1D())
    model.add(Flatten())
    model.add(Dense(8, activation='relu'))
    model.add(Dense(4, activation='tanh'))
    model.add(Dense(4, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    optimizer = Adam(learning_rate=learning_rate)
    model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    return model

model = KerasClassifier(model=create_model, learning_rate=0.0)
batch_size = [16, 32, 64, 128, 256, 512, 1024]
epochs = [10, 20, 50, 100]
learning_rate = [0.1, 0.01, 0.001, 0.0001, 0.00001]
param_grid = dict(batch_size=batch_size, epochs=epochs, learning_rate=learning_rate)
grid = RandomizedSearchCV(estimator=model, param_distributions=param_grid, n_jobs=1, cv=3, pre_dispatch='2*n_jobs')
grid_result = grid.fit(X_train, y_train)

print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))
print('Evaluating best model:')
bestModel = grid_result.best_estimator_
accuracy = bestModel.score(X_test, y_test)
print('accuracy: {:.2f}%'.format(accuracy*100))