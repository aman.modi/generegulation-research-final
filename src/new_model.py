import pandas as pd
from keras.losses import mse
from sklearn.metrics import accuracy_score
from train_test_split import train_test_split_data
from model import create_model
import os
from scikeras.wrappers import KerasClassifier
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sklearn.preprocessing import StandardScaler
from keras import backend as K
from tensorflow.keras.models import load_model
from figure import plot_metric
from keras.callbacks import EarlyStopping, ModelCheckpoint
from load_config_file import load_config
import tensorflow as tf

K.clear_session()
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
early_stopping = EarlyStopping()

data = pd.read_csv('../data/nucleosome_guo2014.csv')
data = data.replace({'positive': 1, 'negative': 0})

X = data.drop(['labels'], axis=1)
y = data['labels']

X_train, X_test, y_train, y_test = train_test_split_data(X, y)

# scaler = StandardScaler()
#
# print(X_train.shape)
# print(X_test.shape)
# X_train = scaler.fit_transform(X_train)
# X_test = scaler.transform(X_test)

gpus = tf.config.list_physical_devices('GPU')
if gpus:
  try:
    # Currently, memory growth needs to be the same across GPUs
    for gpu in gpus:
      tf.config.experimental.set_memory_growth(gpu, True)
    logical_gpus = tf.config.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
  except RuntimeError as e:
    # Memory growth must be set before GPUs have been initialized
    print(e)


model = KerasClassifier(model=create_model, learning_rate=0.0)

# batch_size = [16, 32, 64, 128, 256, 512, 1024]
# epochs = [10, 20, 50, 100]
# learning_rate = [0.1, 0.01, 0.001, 0.0001, 0.00001]
# # dropout = [0.0, 0.1, 0.001, 0.2]
# param_grid = dict(batch_size=batch_size, epochs=epochs, learning_rate=learning_rate)
# grid = RandomizedSearchCV(estimator=model, param_distributions=param_grid, n_jobs=1, cv=3, pre_dispatch='2*n_jobs')
# grid_result = grid.fit(X_train, y_train)
# # summarize results
# print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
# means = grid_result.cv_results_['mean_test_score']
# stds = grid_result.cv_results_['std_test_score']
# params = grid_result.cv_results_['params']
# for mean, stdev, param in zip(means, stds, params):
#     print("%f (%f) with: %r" % (mean, stdev, param))
# print('Evaluating best model:')
# bestModel = grid_result.best_estimator_
# accuracy = bestModel.score(X_test, y_test)
# print('accuracy: {:.2f}%'.format(accuracy*100))