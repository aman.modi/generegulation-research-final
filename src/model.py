from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv1D, Flatten, MaxPooling1D, Dropout
from tensorflow.keras.optimizers import Adam, SGD

def create_model(learning_rate):
    model = Sequential()
    model.add(Conv1D(128, kernel_size=3, input_shape=(574, 1), activation='relu'))
    model.add(MaxPooling1D())
    model.add(Conv1D(64, kernel_size=3, activation='tanh'))
    model.add(MaxPooling1D())
    model.add(Conv1D(64, kernel_size=3, activation='relu'))
    model.add(MaxPooling1D())
    model.add(Conv1D(32, kernel_size=3, activation='tanh'))
    model.add(MaxPooling1D())
    model.add(Conv1D(32, kernel_size=3, activation='relu'))
    model.add(MaxPooling1D())
    model.add(Conv1D(32, kernel_size=3, activation='relu'))
    model.add(MaxPooling1D())
    model.add(Conv1D(16, kernel_size=3, activation='tanh'))
    model.add(MaxPooling1D())
    model.add(Flatten())
    model.add(Dense(8, activation='relu'))
    model.add(Dense(4, activation='tanh'))
    model.add(Dense(4, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    optimizer = SGD(learning_rate=learning_rate)
    model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    return model
