mkdir -p ~/.config/generegulation_research

#Volume configuration
FILE=~/.config/generegulation_research/volumes.cfg
if test -f "$FILE"; then
    echo "Using Volumes defined in $FILE"
else 
    echo "Creating sample volume configuration $FILE"
	cp config/sample_volumes.cfg "$FILE"
fi

printf "\nThese additional Volumes will be mounted in the docker environment:\n"
source config/read_volumes.sh
